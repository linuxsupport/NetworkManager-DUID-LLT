
Summary: NetworkManager DUID-LLT generator workaround
Name: @PACKAGE@
Version: @VERSION@
Release: @RELEASE@%{?dist}
Source0: %{name}-%{version}.tar.gz
Group: CERN/Utilities
BuildRoot: %{_tmppath}/%{name}-%{version}-root
Packager: @EMAIL@
Vendor:  CERN 
License: GPL
Requires: coreutils,gawk,net-tools,sed,grep,bc
Requires: NetworkManager
BuildArch: noarch

%description
NetworkManager-DUID-LLT is a workaround for NetworkManager
generating only DUID-UUID while on CERN network we require
DUID-LL(T) for DHCPv6.

%changelog
* Mon Jul 15 2019 Julien Rische <julien.rische@cern.ch> 0.2-3
- fix false negative exitcode if no DUID-UUID change

* Wed Apr 05 2017 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 0.2-2
- fixed " and \ escaping in generated DUID (thanks V.Brillault)
- made systemd file non-exec.

* Tue Feb 07 2017 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 0.1-2
- initial build 
%prep
%setup 

%build


%install
mkdir -p $RPM_BUILD_ROOT/%{_sbindir}

install -m 755 %{name} $RPM_BUILD_ROOT/%{_sbindir}/%{name}

mkdir -p $RPM_BUILD_ROOT/usr/lib/systemd/system/

install -m 644 %{name}.service $RPM_BUILD_ROOT/usr/lib/systemd/system/

%clean

rm -rf $RPM_BUILD_ROOT

%post 
/bin/systemctl enable %{name} 2>/dev/null
/bin/systemctl start %{name}

%preun
/bin/systemctl --no-reload disable %{name} 2>/dev/null

%files
%defattr(-,root,root)
%doc README
%{_sbindir}/%{name}
/usr/lib/systemd/system/%{name}.service
