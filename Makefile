include VERSION

ifeq ($(PREFIX), $(EMPTY))
PREFIX=/usr/sbin
endif

default:
	@echo "targets: tar srpm brpm rpm clean distclean rpmclean clobber"

stamp-tar:
	rm -rf $(PACKAGE)-$(VERSION)
	mkdir $(PACKAGE)-$(VERSION)
	cp -rv src/* $(PACKAGE)-$(VERSION)/
	find $(PACKAGE)-$(VERSION)/ -name .svn -print0 | xargs -0 rm -rf
	tar --exclude .svn -czf $(PACKAGE)-$(VERSION).tar.gz $(PACKAGE)-$(VERSION)
	rm -rf $(PACKAGE)-$(VERSION)
	touch stamp-tar

tar: stamp-tar

brpm: tar
	make -C rpm brpm

srpm: tar
	make -C rpm srpm

rpm: tar
	make -C rpm rpm

clean:
	@echo "clean happens here"

distclean: clean
	rm -f $(PACKAGE)-$(VERSION).tar.gz
	rm -f stamp-*

rpmclean:
	make -C rpm distclean

clobber: distclean rpmclean

.PHONY: default clean rpmclean stamp-tar
